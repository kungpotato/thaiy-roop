import Link from 'next/link';

export default ({ pathname }) => (
  <header>
    <Link href='/'>
      <a href=''>Home</a>
    </Link>
    <Link href='/about'>
      <a href=''>About</a>
    </Link>
  </header>
)
