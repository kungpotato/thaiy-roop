const functions = require('firebase-functions')
const next = require('next')
// require("firebase/auth");
// require("firebase/firestore");
// require("firebase/functions");

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev, conf: { distDir: 'next' } })
const handle = app.getRequestHandler()

const admin = require('firebase-admin')
var firebaseConfig = {
  apiKey: process.env.FIREBASE_apiKey,
  authDomain: process.env.FIREBASE_authDomain,
  databaseURL: process.env.FIREBASE_databaseURL,
  projectId: process.env.FIREBASE_projectId,
  storageBucket: process.env.FIREBASE_storageBucket,
  messagingSenderId: process.env.FIREBASE_messagingSenderId,
  FIREBASE_appId: process.env.FIREBASE_appId,
}
admin.initializeApp(firebaseConfig)

// const express = require("express");
// const cors = require("cors");
// const app = express();
// const path = require("path");
// app.use(cors({ origin: true }));

exports.next = functions.https.onRequest(async (req, res) => {
  console.log('File: ' + req.originalUrl) // log the page.js file that is being requested
  await app.prepare()
  handle(req, res)
})
